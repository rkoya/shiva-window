export interface LoaderConfig {
    url: string;
    params?: any;
    headers?: Array<any>;
    cache?: boolean;
    data?: any;
}