﻿export class Ease {
    static Linear = "linear";
    static Ease = "ease";
    static EaseIn = "ease-in";
    static EaseOut = "ease-out";
    static EaseInOut = "ease-in-out";
}